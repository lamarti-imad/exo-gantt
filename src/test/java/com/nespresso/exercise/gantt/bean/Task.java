package com.nespresso.exercise.gantt.bean;

import com.nespresso.exercise.gantt.dao.GanttTasksDao;

public abstract class Task {

	private static String UNKNOWN_WEEK = ".";

	private String name;

	private int startWeek;

	private int endWeek;

	private int duration;

	public Task(String name) {
		this.name = name;
	}

	public void defineOccupation(int startWeek, int duration) {
		this.startWeek = startWeek;
		this.duration = duration;
		this.endWeek = (startWeek + duration) - 1;
	}

	public String showPlanFor() {
		StringBuilder planForSB = new StringBuilder();
		if (name.length() > 0) {
			int totalWeek = GanttTasksDao.getInstance().getTotalWeek();
			for (int i = 0; i < totalWeek; i++) {
				if (checkIfIsBetween(totalWeek, i)) {
					planForSB.append(name.charAt(0));
				} else {
					planForSB.append(UNKNOWN_WEEK);
				}
			}
		}
		return planForSB.toString().toUpperCase();
	}

	private boolean checkIfIsBetween(int totalWeek, int week) {
		if (week >= this.startWeek && week <= this.endWeek) {
			return true;
		}
		return false;
	}

	public int getStartWeek() {
		return startWeek;
	}

	public int getDuration() {
		return duration;
	}

}
