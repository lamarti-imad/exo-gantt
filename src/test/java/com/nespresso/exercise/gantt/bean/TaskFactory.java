package com.nespresso.exercise.gantt.bean;

public class TaskFactory {

	public static String INSTALL_DEV = "install dev";
	public static String GOVERNANCE = "governance";
	public static String ANALYSIS = "analysis";
	public static String DEVELOPMENT = "development";

	public static Task getTask(String taskName) {

		if (taskName == null) {
			return null;
		}
		if (taskName.equals(INSTALL_DEV)) {
			return new InstallDev(INSTALL_DEV);
		} else if (taskName.equals(GOVERNANCE)) {
			return new Governance(GOVERNANCE);
		} else if (taskName.equals(ANALYSIS)) {
			return new Analysis(ANALYSIS);
		} else if (taskName.equals(DEVELOPMENT)) {
			return new Development(DEVELOPMENT);
		}

		return null;
	}
}
