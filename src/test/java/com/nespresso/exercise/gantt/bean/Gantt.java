package com.nespresso.exercise.gantt.bean;

import com.nespresso.exercise.gantt.dao.GanttTasksDao;

public class Gantt {

	public Gantt() {
		GanttTasksDao.getInstance().clear();
	}

	public void addTask(String taskName, int startWeek, int endWeek) {

		Task task = TaskFactory.getTask(taskName);

		task.defineOccupation(startWeek, endWeek);

		GanttTasksDao.getInstance().addTask(taskName, task);

	}

	public void addTask(String taskName, int startWeek, int endWeek,
			String secondTaskName) {

	}

	public String showPlanFor(String string) {

		return GanttTasksDao.getInstance().showPlanFor(string);
	}

}
