package com.nespresso.exercise.gantt.dao;

import java.util.HashMap;
import java.util.Map;

import com.nespresso.exercise.gantt.bean.Task;

public class GanttTasksDao {

	private static Map<String, Task> tasks = new HashMap<String, Task>();

	private static GanttTasksDao granttTasksDao = new GanttTasksDao();

	private GanttTasksDao() {
	}

	public static GanttTasksDao getInstance() {
		return granttTasksDao;
	}

	public void addTask(String taskName, Task task) {
		tasks.put(taskName, task);
	}

	public String showPlanFor(String taskName) {
		Task task = tasks.get(taskName);
		return task.showPlanFor();
	}

	public int getTotalWeek() {
		int totalWeek = 0;
		for (Map.Entry<String, Task> entryTask : tasks.entrySet()) {
			int taskDuration = entryTask.getValue().getStartWeek()
					+ entryTask.getValue().getDuration();
			if (taskDuration > totalWeek) {
				totalWeek = taskDuration;
			}
		}
		return totalWeek;
	}

	public void clear() {
		tasks.clear();
	}

}
